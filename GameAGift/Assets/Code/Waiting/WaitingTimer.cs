using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WaitingTimer : MonoBehaviour
{
    [Header ("Real(Hidden)Timer")]
    public float realTimervalue = 30f;
    public float realTimer;
    
    [Header ("Deathcountdown")]
    public float countdown = 120f;
    public float deathTime;
    
    [Header ("Speedup countdown")]
    public float speedFactor = 1f;
    public float speedValue = 2f;
    
    [Header ("Gameobjects")]
    public Text countdownText;
    public GameObject State01;
    public GameObject State02;
    public GameObject State03;


    public Animator arm;
    public AudioSource music;
    public Animator clock;

    void Start()
    {
        realTimer = realTimervalue ;
    }
    
    
    
    // Update is called once per frame
    void Update()
    {
        realTimer -= Time.deltaTime;
        
        countdown -= Time.deltaTime * speedFactor;
        
        countdownText.text = Mathf.Round(countdown).ToString();

        if (Input.GetKeyDown(KeyCode.F))
        {
            speedFactor += speedValue;
            realTimer = realTimervalue;
            arm.SetTrigger("type");
            music.pitch += 0.01f;
            clock.speed += 1.5f;
;        }
        
        if (countdown <= 100)
        {
            State01.SetActive(true);
        }
        
        if (countdown <= 50)
        {
            State02.SetActive(true);
        }
        
        if (countdown <= 20)
        {
            State03.SetActive(true);
        }

        if (countdown <= deathTime)
        {
            SceneManager.LoadScene("BadEndWaiting");
        }

        if (realTimer <= 0)
        {
            SceneManager.LoadScene("GoodEndWaiting");
        }
    }
}
