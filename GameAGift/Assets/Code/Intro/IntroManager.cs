﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour
{
    public AudioSource beep;

    [SerializeField]
    private GameObject[] Bilder;

    [SerializeField] 
    private string Scenename;

    private int introIndex = -1;

    void Start()
    {
        NextFrame();
    }
    
    void Update()
    {
        if (Input.anyKeyDown && introIndex < Bilder.Length)
        {
             NextFrame();
            beep.Play();
        }
    }
    
    public void NextFrame()
    {
        introIndex++;
        
        if (introIndex < Bilder.Length)
        {
            Bilder[introIndex].SetActive(true);
            
            Bilder[introIndex].GetComponent<FadeOnClick>().FadeIn();
            
            Bilder[introIndex].GetComponent<FadeOnClick>().AfterFadeIn();
        }
        
        else if (introIndex >= Bilder.Length)
        {
            SceneManager.LoadScene(Scenename);
            return;
        }
    }
}