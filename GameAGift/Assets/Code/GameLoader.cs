using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


    public class GameLoader : MonoBehaviour
    {
        
        public void BeerPong()
        {
            SceneManager.LoadScene("IntroBeerpong");
        }
        
        public void Grades()
        {
            SceneManager.LoadScene("IntroWaiting");
        }
        
        public void Studienarbeit()
        {
            SceneManager.LoadScene("IntroWriting");
        }

        public void Menu()
        {
            SceneManager.LoadScene("Menu");
        }
        
        public void Intro()
        {
            SceneManager.LoadScene("Intro");
        }
    }

