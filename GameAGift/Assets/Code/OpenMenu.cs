using UnityEngine;

public class OpenMenu : MonoBehaviour
{
    public GameObject menu;
    public void ActivateMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            menu.SetActive(true);
        }
    }
    public void DeactivateMenu()
    {
        menu.SetActive(false);
    }

}

