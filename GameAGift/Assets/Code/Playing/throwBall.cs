using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class throwBall : MonoBehaviour
{
    public Transform firePoint;
    
    public GameObject ballMunition;
    public int munition = 4;
    public TextMeshProUGUI munitionText;
    public GameObject lose;
    public GameObject win;
    
    private Vector3 startPos;
    private Vector3 currentPos;

    public float speed = 10f;

    public GameObject Cup01;
    public GameObject Cup02;
    public GameObject Cup03;

    public bool hit1;
    public bool hit2;
    public bool hit3;

    public AudioSource plong;
    
    
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }
    
    void Update()
    {
        currentPos = startPos;
        currentPos.x = currentPos.x + Mathf.PingPong(Time.time * speed, 8)+1;
        transform.position = currentPos;
        
        
        munitionText.text = "X" + munition.ToString();
        
        if (Input.GetKeyDown(KeyCode.Space) && munition > 0)
        {
            Shoot();
            munition -= 1;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && munition == 0)
        {
            SceneManager.LoadScene(""); //Bad End
        }

        if (!Cup01.activeSelf)
        {
            
            hit1 = true;
            plong.Play();
            speed += 0.001f;
            
            
        }
        
        if (!Cup02.activeSelf)
        {
            
            hit2 = true;
            plong.Play();
            speed += 0.002f;
            
            
        }
        
        if (!Cup03.activeSelf)
        {
             
            hit3 = true;
            plong.Play();
            Debug.Log("Plong");
            speed += 0.001f;
           
           
        }

        if (hit1 && hit2 && hit3)
        {
            SceneManager.LoadScene("GoodEndBeerPong"); //Good end
        }
    }
    
    void Shoot()
    {
        Instantiate(ballMunition, firePoint.position, firePoint.rotation);
    }
}
