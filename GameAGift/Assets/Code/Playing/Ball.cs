using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed;
    private int damage = 1;
    public Rigidbody2D rigidBody;
    
    
    void Start()
    {
        rigidBody.velocity = transform.right * speed;
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Cup cup = hitInfo.GetComponent<Cup>();
        if (cup == null)
        {
            cup = hitInfo.GetComponentInParent<Cup>();
        }

        if (cup != null)
        {
            cup.TakeDamage(damage);
        }
       
        Destroy(gameObject);
        
    }
}
