﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

    public class Cup : MonoBehaviour
    {
        public int health = 1;
        public GameObject destroyObject;
        
     
        public void TakeDamage(int damage)
        {
            health -= damage;

            if (health <= 0)
            {
                Die();
            }
        }
        
        void Die()
        {
           
            Collider2D collider = GetComponent<Collider2D>();
          
            if (collider == null)
            {
                collider = GetComponentInChildren<Collider2D>();
            }

            collider.enabled = false;
            destroyObject.SetActive(false);            
        }
    }

