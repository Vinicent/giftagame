using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimArrowRotation : MonoBehaviour
{
    //public Vector3 defaultRot;
    public float rotFL;

    public float speed = 10f;
    
    
    // Update is called once per frame
    void Update()
    {
        
        transform.rotation =
            Quaternion.Euler(0, 0, Mathf.PingPong(Time.time * speed, rotFL * 2) - rotFL);
       
    }
}
