using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarnMoney : MonoBehaviour
{
    public int treeDamage = 1;

    public int goldDamage = 1;
    
    public int coalDamage = 1;
    
    public Player player;
    
    
    public void destroyTree()
    {
        Player.cashCount += 1;
        player.TakeDamage(treeDamage);
        
    }
    
    public void destroyGold()
    {
        Player.cashCount += 15;
        player.TakeDamage(goldDamage);
        
    }
    
    public void destroyCoal()
    {
        Player.cashCount += 20;
        player.TakeDamage(coalDamage);
        
    }
}
