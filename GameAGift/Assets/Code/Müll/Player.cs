using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    
    [Header("Earth Life")]
    public int maxHealth = 100;
    public int currentHealth;
   // public HealthBar healthBar;

    
    [Header("Cash")]
    public int internalCash;
    public static int cashCount;
    public Text cashText;

    
    
    [Header("Base")]
    public int startLives = 20;
     public static int Lives;
     
    void Start()
    {
        currentHealth = maxHealth;
       // healthBar.SetMaxHealth(maxHealth);

        Lives = startLives;
    }

    void Update()
    {
        internalCash = cashCount;
        cashText.text = " " + internalCash;
    }
    
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
       // healthBar.SetHealth(currentHealth);
        
        
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        SceneManager.LoadScene("Outro 2");
    }
}
