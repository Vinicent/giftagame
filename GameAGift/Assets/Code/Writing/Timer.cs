using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float countdown = 120f;
    public Text countdownText;
    public GameObject lose;
    
    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        
        countdownText.text = Mathf.Round(countdown).ToString();
        
        if (countdown <= 0f)
        {
            SceneManager.LoadScene("BadEndWriting");
        }
    }
}
