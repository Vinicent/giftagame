using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KeyCounter : MonoBehaviour
{
    public Text countText;
    public int internalKey;
    public static int keyCount;
    public GameObject gearOne;
    public GameObject gearTwo;
    public GameObject gearThree;
    public GameObject gearFour;
    public Animator hands;
    public AudioSource typing;
    public bool isPlaying = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        internalKey = keyCount;
        countText.text = " " + keyCount;

        if (Input.anyKeyDown)
        {
            keyCount++;
            hands.SetBool("typing", true);
            if(!typing.isPlaying)
            {
                typing.Play();
            }

        }

        else
        {
            hands.SetBool("typing", false);
            if (!typing.isPlaying)
            {
                typing.Stop();
            }
        }

        if (keyCount >= 1)
        {
            gearOne.SetActive(true);
        }

        if (keyCount >= 500)
        {
            gearTwo.SetActive(true);
            gearOne.SetActive(false);
        }

        if (keyCount >= 1000)
        {
            gearThree.SetActive(true);
            gearTwo.SetActive(false);
        }

        if (keyCount >= 1500)
        {
            SceneManager.LoadScene("GoodEndWriting");
        }


    }
}
