using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnlockWInCanvas : MonoBehaviour
{
    public Canvas wincanvas;

  public void UnlockWinCanvas (AnimationEvent e)
    {
        wincanvas.gameObject.SetActive(true);
        wincanvas.GetComponent<MinigamePrefs>().GameWon();
    }
}
