using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Howtoplay : MonoBehaviour
{
    public GameObject panel;

   public void PanelOn()
    {
        panel.SetActive(true);
    }


    public void PanelOff()
    {
        panel.SetActive(false);
    }
}
