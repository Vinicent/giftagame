using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeactivateClock : MonoBehaviour
{
    public GameObject clock;

    public void NoClock(AnimationEvent e)
    {
        clock.SetActive(false);
    }
}
