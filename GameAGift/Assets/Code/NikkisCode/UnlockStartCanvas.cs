using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockStartCanvas : MonoBehaviour
{
    public Canvas startCanvas;

    public void unlockStartCanvas()
    {
        startCanvas.gameObject.SetActive(true);
    }
}
