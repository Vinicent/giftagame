using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockHomeButton : MonoBehaviour
{
    public GameObject homeButton;
    private void Start()

    {
        bool waitingWon = PlayerPrefs.GetInt("Waiting", 0) > 0;
        bool writingWon = PlayerPrefs.GetInt("Writing", 0) > 0;
        bool playingWon = PlayerPrefs.GetInt("Playing", 0) > 0;

        homeButton.SetActive(waitingWon && writingWon && playingWon);
    }
}
