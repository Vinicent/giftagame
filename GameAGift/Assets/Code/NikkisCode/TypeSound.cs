using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TypeSound : MonoBehaviour
{

    public AudioSource type;

    public void playSound (AnimationEvent e)
    {
        type.Play();
    }
}
