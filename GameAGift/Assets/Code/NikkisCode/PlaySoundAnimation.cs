using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlaySoundAnimation : MonoBehaviour
{

  public AudioSource sound;
  public void PlaySound (AnimationEvent e)
    {
        sound.Play();
    }
}
