using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Animator animator;
    private Vector3 mOffset;
    private float mZCoord;


    //Adding raycast
    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    public void OnPointerEnter(PointerEventData e)
    {
        animator.SetBool("Hover", true);
        Debug.Log("Hover");
    }

    public void OnPointerExit(PointerEventData e)
    {
        animator.SetBool("Hover", false);
    }
}
