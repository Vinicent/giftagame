using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickerUnlocked : MonoBehaviour
{
    public string gameName;
    public GameObject mask;

    private void Start()
    {
        if(PlayerPrefs.GetInt(gameName,0)>0)
        {
            mask.SetActive(false);
        }
    }
}
