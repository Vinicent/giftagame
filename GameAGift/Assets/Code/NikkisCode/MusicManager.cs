using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public bool stopMusic = false;

    void Awake()
    {
        GameObject mm =GameObject.Find("MusicManager");

        if (mm != null)
        {
            DontDestroyOnLoad(mm);
            if(stopMusic)
            {
                mm.GetComponent<AudioSource>().Stop();
                return;
            }

            if(!mm.GetComponent<AudioSource>().isPlaying)
            {
                mm.GetComponent<AudioSource>().Play();
            }
        }
    }


}
