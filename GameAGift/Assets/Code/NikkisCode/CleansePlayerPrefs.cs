using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleansePlayerPrefs : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("Playing", 0);
        PlayerPrefs.SetInt("Waiting", 0);
        PlayerPrefs.SetInt("Writing", 0);
    }

}
